# React Testing Employees Listing

1. This micro-app allows the user to manage company employee details
2. Rewrite components from classes to functions/hooks. Don't worry, this will not take long.
3. Some of the tests will fail, even though your refactor might be correct. Why do these tests fail?
4. Analyse the contents of the tests. What difficulties can you spot? What would you change if you were given time to improve the tests?
