import './App.css';

import employees from './employees/employees.json';
import { EmployeesContainer } from './employees/EmployeesContainer';
import { EmployeesService } from './employees/employees.service';

const svc = new EmployeesService(employees);

function App() {
  return (
    <EmployeesContainer service={svc} />
  );
}

export default App;
