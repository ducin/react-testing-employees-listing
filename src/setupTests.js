// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';

import { configure } from 'enzyme'
// - official support for enzyme stops at react 16
// import Adapter from 'enzyme-adapter-react-16'
// configure({ adapter: new Adapter() })
// - there's no official support for react 17 (at the time of coding this)... but! there's an unofficial package from @wojtekmaj
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
configure({ adapter: new Adapter() });
