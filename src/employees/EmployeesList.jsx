/* eslint-disable */
import React from 'react';

export class EmployeesList extends React.Component{

  static defaultProps = {
    employees: []
  }

  render() {
    return (
      <div className="list-group">
        {this.props.employees.map((employee) => (
          <button
            key={employee.id}
            onClick={(e) => this.props.onSelect(employee)}
            className={`list-group-item ${this.props.selected && (this.props.selected.id === employee.id) ? 'active' : ''}`}
          >
            {employee.name}
          </button>
        ))}
      </div>
    );
  }
}
