/* eslint-disable */
import React from 'react';

export class EmployeeForm extends React.Component {

  static defaultProps = {
    employee: {
      name: '',
      details: ''
    }
  }

  onChange = (e) => {
    let target = e.target;
    let name = target.name;
    let value = target.type === "checkbox" ? target.checked : target.value;

    this.props.onChange({
      ...this.props.employee,
      [name]: value
    });
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.props.onSubmit(this.props.employee);
  }

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <div className="form-group">
          <label className="d-block w-100">
            Name:
            <input
              className="form-control"
              name="name"
              value={this.props.employee.name}
              onChange={this.onChange}
            />
          </label>
        </div>
        <div className="form-group">
          <label className="d-block w-100">
            Employee details:
            <textarea
              className="form-control"
              name="details"
              value={this.props.employee.details}
              onChange={this.onChange}
            />
          </label>
        </div>
        <div className="form-group">
          <input
            type="button"
            id="cancel-employee"
            className="btn btn-default"
            value="Cancel"
            onClick={this.props.onCancel}
          />
          <input
            type="submit"
            id="save-employee"
            className="btn btn-primary ml-2"
            value="Save"
          />
        </div>
      </form>
    );
  }
}
