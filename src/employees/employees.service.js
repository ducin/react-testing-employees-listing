import { v4 as uuid } from 'uuid';

/**
 * In the real application, the service was sending real HTTP requests.
 * This mock service serves the purpose of testing ONLY.
 */

export class EmployeesService {

  constructor(employees) {
    this.employees = employees
  }

  async getEmployees() {
    return this.employees;
  }

  update(updateCb) {
    const clone = [...this.employees];
    updateCb(clone);
    this.employees = clone;
  }

  async saveEmployee(employee) {
    const index = this.employees.findIndex(({ id }) => employee.id === id);

    if (index === -1) {
      employee.id = uuid();
      this.update((employees) => employees.push(employee));
    } else {
      this.update((employees) => employees.splice(index, 1, employee));
    }

    return employee;
  }
}
