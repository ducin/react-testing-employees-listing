/* eslint-disable */
import React from 'react';

import { EmployeesList } from './EmployeesList';
import { EmployeeForm } from './EmployeeForm';

export class EmployeesContainer extends React.Component {

  constructor(props) {
    super(props);

    // EmployeesService object
    this.service = this.props.service;

    this.state = {
      employees: [],
      selected: null
    };

    this.service.getEmployees().then((employees) => {
      this.setState({
        employees
      });
    });
  }

  newEmployee = () => {
    this.setState({
      selected: {
        name: '',
        details: ''
      }
    });
  }

  onSelect = (employee) => {
    this.setState({
      selected: employee
    });
  }

  onSubmit = (employee) => {
    this.service.saveEmployee(employee).then(()=>{
      this.service.getEmployees().then((employees) => {
        this.setState({
          employees
        });
      });
    });
  }

  onCancel = () => {
    this.setState({
      selected: null
    });
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1>Employees</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4">
            <EmployeesList
              employees={this.state.employees}
              selected={this.state.selected}
              onSelect={this.onSelect}
            />
          </div>
          <div className="col-md-4">
            {this.state.selected
            ? (
              <EmployeeForm employee={this.state.selected}
                onChange={this.onSelect}
                onSubmit={this.onSubmit}
                onCancel={this.onCancel}
              />
            ) : (
              <div>
                <button id="new-employee" onClick={this.newEmployee} className="btn btn-primary">New employee</button>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
