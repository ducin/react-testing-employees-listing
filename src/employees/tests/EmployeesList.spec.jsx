import React from 'react';
import { shallow, mount } from 'enzyme';

import employees from '../employees.json';
import { EmployeesList } from '../EmployeesList';

describe('<EmployeesList>', () => {
  it('should be stateless component (no setState, only props)', () => {
    const component = shallow(<EmployeesList employees={employees} />);
    expect(component.state()).toBeNull();
  });

  it('should match snapshot (empty)', () => {
    const component = shallow(<EmployeesList employees={[]} />);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot (content)', () => {
    const component = shallow(<EmployeesList employees={employees} />);
    expect(component).toMatchSnapshot();
  });

  it('should show list of employees', () => {
    const component = shallow(<EmployeesList employees={employees} />);
    expect(component.find('.list-group-item').length).toEqual(employees.length);
  });

  it('should call onSelect function after a employee was clicked', () => {
    const onSelect = jest.fn();
    const component = mount(<EmployeesList employees={employees} onSelect={onSelect} />);
    const item = component.find('.list-group-item').at(1);
    item.simulate('click');

    expect(onSelect.mock.calls[0]).toEqual([employees[1]]);
  });

  it('should add `active` class to a employee after it was selected', () => {
    const employee = employees[1];
    const component = mount(<EmployeesList employees={employees} selected={employee} />);

    const item = component.find('.list-group-item').at(1);
    expect(item.hasClass('active')).toBeTruthy();
  });
});
