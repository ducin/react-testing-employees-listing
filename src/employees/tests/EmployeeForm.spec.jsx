import React from 'react';
import { shallow, mount } from 'enzyme';

import employees from '../employees.json';
import { EmployeeForm } from '../EmployeeForm';

describe('<EmployeeForm>', () => {
  it('should be stateless component (no setState, only props)', () => {
    const component = shallow(<EmployeeForm employee={{}} />);
    expect(component.state()).toBeNull();
  });

  it('should match snapshot', () => {
    const component = shallow(<EmployeeForm employee={{}} />);
    expect(component).toMatchSnapshot();
  });

  it('should display name and details form input fields after a employee is selected', () => {
    const employee = employees[1];
    const component = mount(<EmployeeForm employee={employee} />);
    expect(component.find('[name="name"]').getDOMNode().value).toEqual(employee.name);
    expect(component.find('[name="details"]').getDOMNode().value).toEqual(employee.details);
  });

  it('should call onSubmit with changed employee after the form is submitted', () => {
    const submitSpy = jest.fn();
    const employee = { name: '', details: '' };
    const component = mount(<EmployeeForm employee={employee} onSubmit={submitSpy} />);

    component.find('form [type="submit"]').simulate('click');
    component.find('form').simulate('submit');
    expect(submitSpy.mock.calls[0]).toEqual([employee]);
  });

  it('should call onChange with changed form values', () => {
    const changeSpy = jest.fn();
    const employee = { name: '', details: '' };
    const component = mount(<EmployeeForm employee={employee} onChange={changeSpy} />);

    const nameInput = component.find('[name="name"]');
    nameInput.instance().value = 'test';

    const detailsInput = component.find('[name="details"]');
    detailsInput.instance().value = 'test';

    nameInput.simulate('change', { target: nameInput.getDOMNode() });
    expect(changeSpy.mock.calls[0]).toEqual([{ name: 'test', details: '' }]);

    detailsInput.simulate('change', { target: detailsInput.getDOMNode() });
    expect(changeSpy.mock.calls[1]).toEqual([{ name: '', details: 'test' }]);
  });
})
