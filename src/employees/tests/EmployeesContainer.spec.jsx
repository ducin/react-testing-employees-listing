/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import React from 'react';
import flushPromises from 'flush-promises';
import { act } from "react-dom/test-utils";
import { mount } from 'enzyme';
import { EmployeesContainer } from '../EmployeesContainer';
import { EmployeesService } from '../employees.service';

describe('<EmployeesContainer>', () => {
  function createMockService() {
    const data = [{
      id: '4567', name: 'candidate test', details: 'candidate test'
    }];

    const svc = new EmployeesService(data);
    jest.spyOn(svc, 'getEmployees');
    jest.spyOn(svc, 'saveEmployee');
    return svc;
  }

  it('should match snapshot', () => {
    const mockService = createMockService();
    const component = mount(<EmployeesContainer service={mockService} />);
    expect(component).toMatchSnapshot();
  });

  it('should call EmployeesService.saveEmployee() when form is submitted', () => {
    const mockService = createMockService();
    const component = mount(<EmployeesContainer service={mockService} />);
    expect(mockService.getEmployees.mock.calls.length).toEqual(1);

    component.setState({ selected: mockService.employees[0] });
    component.find('form [type="submit"]').simulate('click');
    component.find('form').simulate('submit');
    expect(mockService.saveEmployee.mock.calls[0]).toEqual([mockService.employees[0]]);
  });

  it('should call EmployeesService.getEmployees() when rendered', () => {
    const mockService = createMockService();
    const component = mount(<EmployeesContainer service={mockService} />);

    expect(mockService.getEmployees.mock.calls.length).toEqual(1);

    return mockService.getEmployees.mock.results[0].value.then(() => {
      component.update();
      expect(component.find('.list-group-item').length).toEqual(mockService.employees.length);
    });
  });

  it('should fetch employees from EmployeesService and update employees list when form is submitted', () => {
    const mockService = createMockService();
    const component = mount(<EmployeesContainer service={mockService} />);
    expect(mockService.getEmployees.mock.calls.length).toEqual(1);

    return mockService.getEmployees.mock.results[0].value.then(() => {
      component.update();
      expect(component.find('.list-group-item').length).toEqual(mockService.employees.length);
    });
  });

  it('should update employee on the list when existing employee is saved', () => {
    const mockService = createMockService();
    const component = mount(<EmployeesContainer service={mockService} />);
    const employee = mockService.employees[0];
    employee.name = 'changed name';

    component.setState({ selected: employee });
    component.find('[name="name"]').simulate('change');

    component.find('form [type="submit"]').simulate('click');
    component.find('form').simulate('submit');

    expect(mockService.saveEmployee.mock.calls.length).toEqual(1);

    // Wait for saveEmployee
    return Promise.resolve(mockService.saveEmployee.mock.results[0].value)
      .then(() => {
        expect(mockService.getEmployees.mock.calls.length).toEqual(2);

        // Wait for second getEmployees
        return mockService.getEmployees.mock.results[1].value;
      })
      .then(() => {
        component.update();
        expect(component.find('.list-group-item').at(0).text()).toBe(employee.name);
      });
  });

  it('should add new employee to list when new employee is saved', () => {
    const mockService = createMockService();
    const component = mount(<EmployeesContainer service={mockService} />);

    component.setState({ selected: { name: 'new', details: 'new' } });
    component.find('form [type="submit"]').simulate('click');
    component.find('form').simulate('submit');

    expect(mockService.saveEmployee.mock.calls.length).toEqual(1);

    // Wait for saveEmployee
    return Promise.resolve(mockService.saveEmployee.mock.results[0].value)
      .then(() => {
        expect(mockService.getEmployees.mock.calls.length).toEqual(2);

        // Wait for second getEmployees
        return mockService.getEmployees.mock.results[1].value;
      })
      .then(() => {
        component.update();
        expect(component.find('.list-group-item').length).toEqual(mockService.employees.length);
      });
  });
});
